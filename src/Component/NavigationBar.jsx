import React, { useState } from "react";

const NavigationBar = () => {
  return (
    <div className="d-flex">
      <div
        className="px-3 py-1"
        style={{ backgroundColor: "#0D28A6", height: "600px", width: "100px" }}
      >
        <div
          style={{ width: "97%", height: "40px", backgroundColor: "#E5E5E5" }}
        ></div>
        <img src="./Assets/Home.png" alt=" " />
        <img src="./Assets/Administrator.png" alt=" " />
      </div>
    </div>
  );
};
export default NavigationBar;
