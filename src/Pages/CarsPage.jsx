import React from "react";
import FetchProduct from "../Component/fetchProduct";
import NavigationBar from "../Component/NavigationBar";
import NavigationHeader from "../Component/NavigationHeader";
import { useLocation } from "react-router-dom";

const Cars = () => {
  const location = useLocation();
  const data = location.state;
  return (
    <div className="d-flex">
      <div className="flex-1" style={{ position: "fixed" }}>
        <NavigationBar />
      </div>
      <div
        className="flex-1 px-1 py-1"
        style={{
          backgroundColor: "white",
          height: "600px",
          width: "100px",
          marginLeft: "100px",
          position: "fixed",
        }}
      >
        <div
          style={{ width: "97%", height: "40px", backgroundColor: "#E5E5E5" }}
        ></div>
        <h5 style={{ color: "grey", paddingTop: "70px" }}>Car</h5>
        <h5>List Car</h5>
      </div>
      <div className="flex-10" style={{ marginLeft: "200px" }}>
        <NavigationHeader />
        <FetchProduct props={data} />
      </div>
    </div>
  );
};
export default Cars;
